package in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidraver.page;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import android.view.GestureDetector;

import in.ac.srmuniv.guidrawer.R;
import in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidrawer.recycler.CustomTableLayout;
import in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidrawer.recycler.Menu_Recycler_View_Adapter;
import in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidrawer.recycler.TableMainLayout;
import in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidrawer.recycler.TableType;
import in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidrawer.recycler.Table_Recycler_View_Adapter;


public class PageFragment extends Fragment {
    private static final String ARG_PAGE_NUMBER = "page_number";
    public TabName tabName;

    public PageFragment() {

    }

    public static PageFragment newInstance(int page) {

        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_page_layout, container, false);
        Bundle args = this.getArguments();
        int pageNumber = args.getInt("page_number");
        tabName = TabName.values()[pageNumber - 1];



        if (tabName != null) {

            switch (tabName) {
                case Menu: {
                    List<DataMenu> data = fill_with_data();
                    RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
                    final Menu_Recycler_View_Adapter adapter = new Menu_Recycler_View_Adapter(data, getContext());
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    break;
                }
                case Tasks: {
                    List<DataTable> data = fill_with_data_table();
                    RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
                    final Table_Recycler_View_Adapter adapter = new Table_Recycler_View_Adapter(data, getContext(), TableType.Portrait);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    break;
                }
                case Option: {
                    TableLayout table = createTable(getContext());

                    ScrollView sv = new ScrollView(getContext());
                    sv.addView(table);

                    return sv;

                }
                case Messages: {
                    TableMainLayout customTableLayout = new TableMainLayout(getContext());
                    return customTableLayout;

                }
                case Process: {
                    List<DataTable> data = fill_with_data_table();
                    RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
                    final Table_Recycler_View_Adapter adapter = new Table_Recycler_View_Adapter(data, getContext(), TableType.Landscape);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    break;
                }
                default: {
                    List<DataMenu> data = fill_with_data();
                    RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
                    final Menu_Recycler_View_Adapter adapter = new Menu_Recycler_View_Adapter(data, getContext());
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                }
            }
        }

        return rootView;
    }

    public List<DataTable> fill_with_data_table() {
        List<DataTable> data = new ArrayList<>();
        switch (tabName) {
            case Tasks: {
                data.add(new DataTable("Položka1", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                data.add(new DataTable("Položka2", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                data.add(new DataTable("Položka3", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                data.add(new DataTable("Položka4", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                break;
            }
            case Process: {
                data.add(new DataTable("Položka1", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                data.add(new DataTable("Položka2", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                data.add(new DataTable("Položka3", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                data.add(new DataTable("Položka4", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                break;
            }

            case Messages: {
                data.add(new DataTable("Položka1", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                data.add(new DataTable("Položka2", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                data.add(new DataTable("Položka3", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                data.add(new DataTable("Položka4", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));

                break;
            }
            default: {
                data.add(new DataTable("Položka1", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                data.add(new DataTable("Položka2", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                data.add(new DataTable("Položka3", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));
                data.add(new DataTable("Položka4", R.drawable.ic_action_movie, "KK0000364", "N", "Zásilkové balení", "COMTEL", "22001", "na skladě", "dopravovano", "prioritní"));

            }
        }

        return data;
    }



    public List<DataMenu> fill_with_data() {
        List<DataMenu> data = new ArrayList<>();


        switch (tabName) {
            case Menu: {
                data.add(new DataMenu("Výdej", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                data.add(new DataMenu("Sklad", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                data.add(new DataMenu("Fakturace", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                data.add(new DataMenu("Externí sklad", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                data.add(new DataMenu("Meziskladové převody", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                break;
            }

            case Option: {
                data.add(new DataMenu("Options", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                data.add(new DataMenu("Sklad", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                data.add(new DataMenu("Fakturace", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                data.add(new DataMenu("Externí sklad", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                data.add(new DataMenu("Meziskladové převody", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                break;
            }


            default: {
                data.add(new DataMenu("default", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                data.add(new DataMenu("Sklad", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                data.add(new DataMenu("Fakturace", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                data.add(new DataMenu("Externí sklad", "Vyberte z následujících možností", R.drawable.ic_action_movie));
                data.add(new DataMenu("Meziskladové převody", "Vyberte z následujících možností", R.drawable.ic_action_movie));

            }


        }
        return data;
    }

    public TableLayout createTable (Context c ) {


        CustomTableLayout mTableLayout = new CustomTableLayout(c);
        return mTableLayout;
    }


}