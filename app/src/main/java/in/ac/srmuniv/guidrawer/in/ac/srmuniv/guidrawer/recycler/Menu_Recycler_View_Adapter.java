package in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidrawer.recycler;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidraver.page.DataMenu;
import in.ac.srmuniv.guidrawer.R;


public class Menu_Recycler_View_Adapter extends RecyclerView.Adapter<Menu_View_Holder> {

    List<DataMenu> list = Collections.emptyList();
    Context context;
    ArrayList<String> buttons;



    public Menu_Recycler_View_Adapter(List<DataMenu> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public Menu_View_Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        buttons = new ArrayList<String>();
        buttons.add("externí");
        buttons.add("interní");
        buttons.add("ještě nějaké tlačítko");
        buttons.add("sklad ");
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_row_layout, parent, false);
        final Menu_View_Holder holder = new Menu_View_Holder(v, new Menu_View_Holder.IMyViewHolderClicks() {
            public void titleCaller(Menu_View_Holder holder) {
                if (holder.isOpen) {
                    holder.description.setVisibility(View.VISIBLE);
                    holder.buttonLayout.setVisibility(LinearLayout.VISIBLE);
                } else {
                    holder.description.setVisibility(View.GONE);
                    holder.buttonLayout.setVisibility(LinearLayout.GONE);
                }
                holder.isOpen = !holder.isOpen;

            }
            public void descriptionCaller(TextView description) { Log.d("VEGETABLES", "To-m8-tohs"); }
        });

        return holder;

    }


    @Override
    public void onBindViewHolder(Menu_View_Holder holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.title.setText(list.get(position).title);
        holder.description.setText(list.get(position).description);
        holder.position = position;

        holder.imageView.setImageResource(list.get(position).imageId);

        for (String name : buttons) {
            holder.addButton(name, context);
        }


   //     animate(holder);



    /*    holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView title = (TextView) v;
                title.setText("blabla");
                Toast.makeText(v.getContext(), "inside", Toast.LENGTH_SHORT).show();
            }
        });*/

    }


    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.bounce_interpolator);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, DataMenu dataMenu) {
        list.add(position, dataMenu);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified DataMenu object
    public void remove(DataMenu dataMenu) {
        int position = list.indexOf(dataMenu);
        list.remove(position);
        notifyItemRemoved(position);
    }



}
