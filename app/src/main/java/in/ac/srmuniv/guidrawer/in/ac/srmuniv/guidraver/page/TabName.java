package in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidraver.page;

public enum TabName {
    Menu("Menu"),
    Process("Rozpracované"),
    Tasks("Úkoly"),
    Messages("Zprávy"),
    Option("Možnosti");

    private String name;

    TabName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
