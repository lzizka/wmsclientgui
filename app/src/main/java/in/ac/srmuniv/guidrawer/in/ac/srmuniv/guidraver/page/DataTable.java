package in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidraver.page;

public class DataTable {
    public String title;
    public int imageId;
    public String podklad;
    public String p;
    public String funkce;
    public String organizace;
    public String sklad;
    public String sklad2;
    public String doprava;
    public String priorita;

    DataTable(String title, int imageId, String podklad, String p, String funkce, String organizace, String sklad, String sklad2, String doprava, String priorita) {
        this.title = title;
        this.imageId = imageId;
        this.podklad = podklad;
        this.p = p;
        this.funkce = funkce;
        this.organizace = organizace;
        this.sklad = sklad;
        this.sklad2 = sklad2;
        this.doprava = doprava;
        this.priorita = priorita;
    }

}