package in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidraver.page;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;



public class TabsPagerAdapter extends FragmentPagerAdapter {
    ArrayList<String>titlePage;

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);

       titlePage = new ArrayList<>();
       titlePage.add(TabName.Menu.getName());
       titlePage.add(TabName.Process.getName());
       titlePage.add(TabName.Tasks.getName());
       titlePage.add(TabName.Messages.getName());
       titlePage.add(TabName.Option.getName());
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.newInstance(position + 1);
    }

    @Override
    public int getCount() {
        return titlePage.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return titlePage.get(position);
    }
}