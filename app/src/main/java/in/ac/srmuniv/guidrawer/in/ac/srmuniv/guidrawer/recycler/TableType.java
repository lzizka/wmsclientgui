package in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidrawer.recycler;

public enum TableType {
    Portrait,
    Landscape
}
