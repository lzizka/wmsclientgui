package in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidrawer.recycler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import in.ac.srmuniv.guidrawer.R;

public class CustomTableLayout extends TableLayout {

    public CustomTableLayout(final Context context) {

        super(context);





        int rowc = 30;
        int col = 5;

        final AlertDialog dialog = new AlertDialog.Builder(getContext()).create();
        final EditText editText = new EditText(getContext());
        dialog.setTitle("This is Edit");
        dialog.setView(editText);




        for (int count = 0; count < rowc; count++) {
            TableRow row = new TableRow(context);


            for (int countl = 0; countl < col; countl++) {
                final TextView valueTV = new TextView(context);

                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Save Text", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        valueTV.setText(editText.getText());

                    }
                });

                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        hideKeyboardFrom(context, getRootView());
                    }
                });

                valueTV.setText("text: "+ count + countl + " ");
                GradientDrawable gd = new GradientDrawable();
                if (count % 2 != 1) {
                    gd.setColor(Color.rgb( 255, 241, 118));
                } else {
                    gd.setColor(Color.rgb( 220, 231, 117));
                }

                gd.setCornerRadius(1);
                gd.setStroke(1, Color.DKGRAY);
                valueTV.setGravity(Gravity.CENTER_HORIZONTAL);
                valueTV.setBackground(gd);
                valueTV.setWidth(getScreenWidth(context) / col);

                valueTV.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        editText.setText(valueTV.getText());
                        dialog.show();
                        return true;
                    }
                });

                row.addView(valueTV);
            }

            this.addView(row);
        }
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
