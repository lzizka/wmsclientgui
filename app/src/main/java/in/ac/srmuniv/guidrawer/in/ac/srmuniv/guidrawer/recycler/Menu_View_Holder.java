package in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidrawer.recycler;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.ac.srmuniv.guidrawer.R;

public class Menu_View_Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

    CardView cv;
    TextView title;
    TextView description;
    ImageView imageView;
    LinearLayout buttonLayout;
    public IMyViewHolderClicks mListener;
    boolean isOpen;
    int position;

    Menu_View_Holder(View itemView, IMyViewHolderClicks listener) {
        super(itemView);
        cv = (CardView) itemView.findViewById(R.id.cardView);
        title = (TextView) itemView.findViewById(R.id.title);
        description = (TextView) itemView.findViewById(R.id.description);
        imageView = (ImageView) itemView.findViewById(R.id.imageView);
        buttonLayout = (LinearLayout) itemView.findViewById(R.id.buttonLayout);
        description.setVisibility(View.GONE);
        buttonLayout.setVisibility(LinearLayout.GONE);
        title.setOnClickListener(this);
        description.setOnClickListener(this);
        imageView.setOnClickListener(this);
        cv.setOnClickListener(this);
        mListener = listener;
        isOpen = true;
    }

    @Override
    public void onClick(View v) {
        if (v == title){
            mListener.titleCaller(this);

        } else if (v == description){
            mListener.descriptionCaller((TextView)v);

        } else if (v == cv) {
            mListener.titleCaller(this);
        }
    }

    public void addButton (String name, Context c) {
        Button btn = new Button(c);
        btn.setText(name);
        buttonLayout.addView(btn);
    }

    public static interface IMyViewHolderClicks {
        public void titleCaller(Menu_View_Holder holder);
        public void descriptionCaller(TextView description);
    }
}
