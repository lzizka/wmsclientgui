package in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidraver.page;

public class DataMenu {
    public String title;
    public String description;
    public int imageId;

    DataMenu(String title, String description, int imageId) {
        this.title = title;
        this.description = description;
        this.imageId = imageId;
    }

}
