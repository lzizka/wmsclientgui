package in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidrawer.recycler;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import android.widget.TextView;

import java.util.Collections;
import java.util.List;


import in.ac.srmuniv.guidrawer.R;
import in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidraver.page.DataTable;


public class Table_Recycler_View_Adapter extends RecyclerView.Adapter<Table_View_Holder> {

    List<DataTable> list = Collections.emptyList();
    Context context;
    TableType type;



    public Table_Recycler_View_Adapter(List<DataTable> list, Context context, TableType type) {
        this.list = list;
        this.context = context;
        this.type = type;
    }

    @Override
    public Table_View_Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v;
    if (type == TableType.Landscape) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_row_layout_landscape, parent, false);
    } else {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_row_layout, parent, false);
    }

        final Table_View_Holder holder = new Table_View_Holder(v, new Table_View_Holder.IMyViewHolderClicksTable() {
            public void titleCaller(Table_View_Holder holder) {
                if (holder.isOpen) {
                    holder.valueLayout.setVisibility(View.VISIBLE);
                } else {
                    holder.valueLayout.setVisibility(View.GONE);
                }
                holder.isOpen = !holder.isOpen;
            }
            public void descriptionCaller(TextView description) { Log.d("VEGETABLES", "To-m8-tohs"); }
        });


        return holder;
    }


    @Override
    public void onBindViewHolder(Table_View_Holder holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.title.setText(list.get(position).title);
        holder.podklad.setText(list.get(position).podklad);
        holder.p.setText(list.get(position).p);
        holder.funkce.setText(list.get(position).funkce);
        holder.organizace.setText(list.get(position).organizace);
        holder.sklad.setText(list.get(position).sklad);
        holder.sklad2.setText(list.get(position).sklad2);
        holder.doprava.setText(list.get(position).doprava);
        holder.priorita.setText(list.get(position).priorita);
        holder.position = position;
        holder.imageView.setImageResource(list.get(position).imageId);


    }


    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.bounce_interpolator);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, DataTable dataTable) {
        list.add(position, dataTable);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified DataMenu object
    public void remove(DataTable dataTable) {
        int position = list.indexOf(dataTable);
        list.remove(position);
        notifyItemRemoved(position);
    }



}
