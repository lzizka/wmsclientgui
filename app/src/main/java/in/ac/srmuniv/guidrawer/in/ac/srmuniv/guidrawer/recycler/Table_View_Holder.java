package in.ac.srmuniv.guidrawer.in.ac.srmuniv.guidrawer.recycler;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.ac.srmuniv.guidrawer.R;

public class Table_View_Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

    CardView cv;
    EditText title;
    EditText podklad;
    EditText p;
    EditText funkce;
    EditText organizace;
    EditText sklad;
    EditText sklad2;
    EditText doprava;
    EditText priorita;
    ImageView imageView;
    LinearLayout valueLayout;
    public IMyViewHolderClicksTable mListener;
    boolean isOpen;
    int position;

    Table_View_Holder(View itemView, IMyViewHolderClicksTable listener) {
        super(itemView);
        cv = (CardView) itemView.findViewById(R.id.cardView);
        title = (EditText) itemView.findViewById(R.id.title);
        podklad = (EditText) itemView.findViewById(R.id.podklad);
        p = (EditText) itemView.findViewById(R.id.p);
        funkce = (EditText) itemView.findViewById(R.id.funkce);
        organizace = (EditText) itemView.findViewById(R.id.organizace);
        sklad = (EditText) itemView.findViewById(R.id.sklad);
        sklad2 = (EditText) itemView.findViewById(R.id.sklad2);
        doprava = (EditText) itemView.findViewById(R.id.doprava);
        priorita = (EditText) itemView.findViewById(R.id.priorita);

        imageView = (ImageView) itemView.findViewById(R.id.imageView);
        valueLayout = (LinearLayout) itemView.findViewById(R.id.valueLayout);

        valueLayout.setVisibility(LinearLayout.VISIBLE);
        title.setOnClickListener(this);

        imageView.setOnClickListener(this);
        cv.setOnClickListener(this);
        mListener = listener;
        isOpen = false;
    }

    @Override
    public void onClick(View v) {
        if (v == title){
           // title.setEnabled(!title.isEnabled());
            mListener.titleCaller(this);

        }  else if (v == cv) {
            mListener.titleCaller(this);
        }
    }


    public interface IMyViewHolderClicksTable {
        void titleCaller(Table_View_Holder holder);
        void descriptionCaller(TextView description);
    }
}
